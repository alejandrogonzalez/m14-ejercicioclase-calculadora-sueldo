package com.example.exercicicalculadorasou;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    private EditText editTextHores;
    private Button button;
    private TextView textViewSou;
    private Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextHores = findViewById(R.id.editTextHores);
        button = findViewById(R.id.button);
        textViewSou = findViewById(R.id.textViewSou);

        spinner = findViewById(R.id.spinner);
        spinner.getSelectedItemPosition();

        editTextHores.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == 66){
                    calcularSou();
                }
                return false;
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcularSou();



            }
        });
    }

    public void calcularSou(){

        String hores = editTextHores.getText().toString();
        if (hores.isEmpty()){
            Toast.makeText(MainActivity.this, "Introdueix número d'hores",Toast.LENGTH_LONG).show();
        }
        else {
            int numHores = Integer.parseInt(hores);
            int sou;
            if (numHores <= 40){
                sou = numHores * 16;
            }else{
                sou = (40 * 16) + (numHores - 40) * 20;
            }
            textViewSou.setText(sou + " €");
            textViewSou.setVisibility(View.VISIBLE);

    }
}
}